# Meta filer

## Setup

1. Create a virtual environment and activate it
2. Install requirements: `pip install -r requirements.txt`
3. Install ExifTool and PyExifTool:

    3.1 ExifTool:
        - Go to https://exiftool.org/ and download the tar file
        - unpack the tar file and make it your current directory
        - type on terminal
            $ perl Makefile.PL
            $ make test
            $ sudo make install
        (Note: The "make test" step is not required, but useful because it
        runs a full suite of tests to verify that ExifTool is working properly
        on your system.)

    Complete installations instructions: https://exiftool.org/install.html#Unix

    3.2 PyExifTool:
        - Go to: https://pypi.org/project/PyExifTool/ and download the tar file.
        - extract the tarball file in the project directory,
        - type on terminal
            $ python setup.py install
            $ python -m unittest -v tests/test_exiftool.py

    Note: tarball files can be found in tarball_files_needed/

4. Run a local server: `python manage.py runserver


#
## info

for extracting-metadata endpoint
In order to reduce the amount of load of files, I divided the models by some
of the most common file-types, and I added an extraction_id that differentiate in which model
I save the metadata.

for the Get-metadata endpoint
Thanks to the extraction_id define as: IMG_1, TXT_1, AUDIO_1, VIDEO_1, VARIOUS_1
Now is really easy to get the metadata fro the exact model without doing heavy statements or queries.
eg: if extraction_id is IMG_236, I will check extraction_id[:3],
to make sure which model I'm going to look, which is ImageMetadata in this case,
then just one query in one model.

for the query-metadata endpoint
I have two cases here:
 1) the tag='file_extension' or tag='file_mime_type'
 then for the extension that could be: png, mp4, mp3, jpg etc, and for the
 file_mime_type: image/jpg, text/plain, video/mp4, image/png etc etc
 in which I would check those types and get the exact model where I will do the query.
 then again just one query and one model.

 2) the rest of the tags, in which I will need to do the query in every model.
 eg: if tag='file_path', I will need to check the file_path in ImageMetadata, TextMetadata etc.


## How to extend
    1) Make that extract_metadata could get more than one file at a time.
    - For this we need to implement a bulk_create in order to speed up the load of files,
    for which we need to create a batch, and doing it with itertools.zip_longest()
    which work as a generator function and yield the elements instead of loading it on memory.


    2) in the endpoint QueryMetadata, I created some sets in order to know in which model
       I could check depending of the file_type, but for sure I'm gonna miss some types
       doing it like that, so a better way that I think to do that is creating models for
       extensions, so every time a new metadata file is saved, it will check if the extension of
       that file it's saved already, that way every file stored, it will have the extension saved.
       ImageMetadata objects could be millions, but in the ImageExtensions they could be hundreds only.


## Endpoints

 - Extrac metadata
        http://127.0.0.1:8000/file_processor/api/upload

   - Not working because of how I'm trying to pass the file to exiftool,
     (utils.py line 18).
     I think I have to save the file locale, and then pass the location to
     exiftool.

 -Get metadata
        http://127.0.0.1:8000/file_processor/api/get_metadata
        http://127.0.0.1:8000/file_processor/api/get_metadata?extraction_id=IMG_3

    -Working because I created file metadata in all models manually by
     python manage.py shell

 - Query metadata
        http://127.0.0.1:8000/file_processor/api/query_metadata
        http://127.0.0.1:8000/file_processor/api/query_metadata?tag=file_path&value=downloads

    -Working because I created file metadata in all models manually by
     python manage.py shell


#
## Testing
Running `tox` will run:
  - pytest
  - flake8
  - isort
  - coverage

For testing and PEP8 style guidelines.

###Being in root of project/ directory, Do:

    $ tox


This will run all at once.
##


###In order to run each command individually just run:
###Just run:

- for tests:

      $ pytest

- flake8:

      $ pytest --flake8

- Isort:

      $ pytest --isort

- Coverage:

      $ pytest --cov-config=tox.ini --cov=event_manager
#
#
#
#