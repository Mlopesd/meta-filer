from rest_framework import serializers

from meta_filer.file_processor.models import (AudioMetadata, ImageMetadata,
                                              TextMetadata, VariousMetadata,
                                              VideoMetadata)


class FileSerializer(serializers.Serializer):
    file = serializers.FileField()


class GetMetadataSerializer(serializers.Serializer):
    date_created = serializers.DateField()
    date_modified = serializers.DateField()
    file_path = serializers.CharField(max_length=140)
    file_name = serializers.CharField(max_length=140)
    file_extension = serializers.CharField(max_length=140)
    file_mime_type = serializers.CharField(max_length=140)


class ImageMetadataSerializer(serializers.ModelSerializer):
    class Meta:
        model = ImageMetadata
        fields = (
            'date_created', 'date_modified', 'file_path',
            'file_name', 'file_extension', 'file_mime_type'
        )


class VideoMetadataSerializer(serializers.ModelSerializer):
    class Meta:
        model = VideoMetadata
        fields = (
            'date_created', 'date_modified', 'file_path',
            'file_name', 'file_extension', 'file_mime_type'
        )


class AudioMetadataSerializer(serializers.ModelSerializer):
    class Meta:
        model = AudioMetadata
        fields = (
            'date_created', 'date_modified', 'file_path',
            'file_name', 'file_extension', 'file_mime_type'
        )


class TextMetadataSerializer(serializers.ModelSerializer):
    class Meta:
        model = TextMetadata
        fields = (
            'date_created', 'date_modified', 'file_path',
            'file_name', 'file_extension', 'file_mime_type'
        )


class VariousMetadataSerializer(serializers.ModelSerializer):
    class Meta:
        model = VariousMetadata
        fields = (
            'date_created', 'date_modified', 'file_path',
            'file_name', 'file_extension', 'file_mime_type'
        )
