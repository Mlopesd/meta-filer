from datetime import datetime

import exiftool

from meta_filer.file_processor.models import (AudioMetadata, ImageMetadata,
                                              TextMetadata, VariousMetadata,
                                              VideoMetadata)

# from django.core.files import File


def get_metadata(file):
    """
        Return a dict with the metadata of the specific file_type
    """

    with exiftool.ExifTool() as et:
        file_metadata = et.get_metadata(file)
        # file_metadata = et.get_metadata(File(file))

    metadata = {}
    # ------ Created-date variations------------- #
    if 'EXIF:DateTimeOriginal' in file_metadata:
        metadata['date_created'] = file_metadata['EXIF:DateTimeOriginal']
    elif 'video' in metadata['File:MIMEType'] and 'H264:DateTimeOriginal' in file_metadata:
        metadata['date_created'] = file_metadata['H264:DateTimeOriginal']
    elif 'Torrent:CreateDate' in file_metadata:
        metadata['date_created'] = file_metadata['Torrent:CreateDate']
    elif 'QuickTime:CreateDate' in file_metadata:
        metadata['date_created'] = file_metadata['QuickTime:CreateDate']
    elif 'PDF:CreateDate' in file_metadata:
        metadata['date_created'] = file_metadata['PDF:CreateDate']
    elif 'File:FileModifyDate' in file_metadata:
        metadata['date_created'] = file_metadata.get('File:FileModifyDate') \
                                   or datetime.now().strftime('%Y-%m-%d')

    # ------ Modified-date variations------------- #
    if 'EXIF:ModifyDate' in file_metadata:
        metadata['date_modified'] = file_metadata['EXIF:ModifyDate']
    elif 'QuickTime:ModifyDate' in file_metadata:
        metadata['date_modified'] = file_metadata['QuickTime:ModifyDate']
    elif 'PDF:ModifyDate' in file_metadata:
        metadata['date_modified'] = file_metadata['PDF:ModifyDate']
    elif 'File:FileModifyDate' in file_metadata:
        metadata['date_modified'] = file_metadata.get('File:FileModifyDate') \
                                    or datetime.now().strftime('%Y-%m-%d')

    # ------ rest of the metadata------------- #
    metadata['file_path'] = file_metadata.get('File:Directory') or 'file path not provided'
    metadata['file_name'] = file_metadata.get('File:FileName') or 'file name not provided'
    metadata['file_mime_type'] = file_metadata.get('File:MIMEType') or 'mime type not provided'

    # The files: .py, .c, .md, .ini, between others,
    # in 'file_extension' metadata, they all will return 'TXT'
    # with this we are making sure it will save the right file extension.
    if metadata['file_extension'].lower() == metadata['file_name'].split('.')[-1].lower():
        metadata['file_extension'] = file_metadata['File:FileTypeExtension'].lower()
    else:
        metadata['file_extension'] = metadata['file_name'].split('.')[-1].lower()

    # ------ casting dates ------------- #
    # because we could find dates like this: 2021:10:13 16:18:23.596299+01:00
    # we are going to slice it like this: metadata['date_created'][:10]
    # and get: 2021:10:13, then cast.
    date_created = datetime.strptime(
        metadata['date_created'][:10], '%Y:%m:%d'
    ).strftime('%Y-%m-%d')
    metadata['date_created'] = date_created

    date_modified = datetime.strptime(
        metadata['date_modified'][:10], '%Y:%m:%d'
    ).strftime('%Y-%m-%d')
    metadata['date_modified'] = date_modified

    return metadata


def save_file_metadata(file):
    """
    Depending on the file_type, it will save the metadata of the file
    in the right model, but if file_type is unknown it will save the
    metadata in VariousMetadata model.

    File ty for each model:
    ImageMetadata => jpg, png, raw, gif, tiff, heic, etc.
    TextMetadata => txt, py, c, ini, pdf, md, csv, odt, math sheet, docx, etc.
    AudioMetadata => m4a, mp3, wav, wma, aac, flac etc.
    VideoMetadata => mkv, mp4, mov, wmv, avi, flv, webm, mpeg-2, avchd etc.
    VariousMetadata, zip, bittorrent, run, etc.
    """
    metadata = get_metadata(file)

    if 'text' in metadata['File:MIMEType'].lower() or 'pdf' in metadata['File:MIMEType'].lower():
        TextMetadata.objects.get_or_create(
            date_created=metadata['date_created'].lower(),
            date_modified=metadata['date_modified'].lower(),
            file_path=metadata['file_path'].lower(),
            file_name=metadata['file_name'].lower(),
            file_extension=metadata['file_extension'].lower(),
            file_mime_type=metadata['file_mime_type'].lower(),
        )
    elif 'image' in metadata['File:MIMEType'].lower():
        ImageMetadata.objects.get_or_create(
            date_created=metadata['date_created'].lower(),
            date_modified=metadata['date_modified'].lower(),
            file_path=metadata['file_path'].lower(),
            file_name=metadata['file_name'].lower(),
            file_extension=metadata['file_extension'].lower(),
            file_mime_type=metadata['file_mime_type'].lower(),
        )
    elif 'video' in metadata['File:MIMEType'].lower():
        VideoMetadata.objects.get_or_create(
            date_created=metadata['date_created'].lower(),
            date_modified=metadata['date_modified'].lower(),
            file_path=metadata['file_path'].lower(),
            file_name=metadata['file_name'].lower(),
            file_extension=metadata['file_extension'].lower(),
            file_mime_type=metadata['file_mime_type'].lower(),
        )
    elif 'audio' in metadata['File:MIMEType'].lower():
        AudioMetadata.objects.get_or_create(
            date_created=metadata['date_created'].lower(),
            date_modified=metadata['date_modified'].lower(),
            file_path=metadata['file_path'].lower(),
            file_name=metadata['file_name'].lower(),
            file_extension=metadata['file_extension'].lower(),
            file_mime_type=metadata['file_mime_type'].lower(),
        )
    else:
        VariousMetadata.objects.get_or_create(
            date_created=metadata['date_created'].lower(),
            date_modified=metadata['date_modified'].lower(),
            file_path=metadata['file_path'].lower(),
            file_name=metadata['file_name'].lower(),
            file_extension=metadata['file_extension'].lower(),
            file_mime_type=metadata['file_mime_type'].lower(),
        )
