from django.urls import include, path
from rest_framework.routers import DefaultRouter

from meta_filer.file_processor.views import (FileView, GetMetadata,
                                             QueryMetadata)

router = DefaultRouter()

app_name = 'ejercicio'
urlpatterns = [
    path('api', include(router.urls)),
    path('api/upload', FileView.as_view()),
    path('api/get_metadata/', GetMetadata.as_view()),
    path('api/query_metadata/', QueryMetadata.as_view())
]
