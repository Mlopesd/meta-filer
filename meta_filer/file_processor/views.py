from drf_multiple_model.pagination import MultipleModelLimitOffsetPagination
from drf_multiple_model.views import ObjectMultipleModelAPIView
from rest_framework import status
from rest_framework.parsers import (FileUploadParser, FormParser,
                                    MultiPartParser)
from rest_framework.response import Response
from rest_framework.views import APIView

from meta_filer.file_processor.models import (AudioMetadata, ImageMetadata,
                                              TextMetadata, VariousMetadata,
                                              VideoMetadata)
from meta_filer.file_processor.serializers import (AudioMetadataSerializer,
                                                   FileSerializer,
                                                   GetMetadataSerializer,
                                                   ImageMetadataSerializer,
                                                   TextMetadataSerializer,
                                                   VariousMetadataSerializer,
                                                   VideoMetadataSerializer)
from meta_filer.file_processor.utils import save_file_metadata


class FileView(APIView):
    parser_classes = (MultiPartParser, FormParser, FileUploadParser)

    def post(self, request, *args, **kwargs):
        file_serializer = FileSerializer(data=request.data)
        if file_serializer.is_valid():
            save_file_metadata(file_serializer)
            return Response(file_serializer.data, status=status.HTTP_201_CREATED)
        else:
            return Response(file_serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class GetMetadata(APIView):
    def get(self, request, *args, **kwargs):
        query = request.query_params.get('extraction_id')

        # extraction_id looks like: IMG_1, TXT_1, AUDIO_1, VIDEO_1, VARIOUS_1
        if query[:3].lower() == 'img':
            metadata = ImageMetadata.objects.filter(extraction_id=query)[0]
        elif query[:3].lower() == 'txt':
            metadata = TextMetadata.objects.filter(extraction_id=query)[0]
        elif query[:3].lower() == 'aud':
            metadata = AudioMetadata.objects.filter(extraction_id=query)[0]
        elif query[:3].lower() == 'vid':
            metadata = VideoMetadata.objects.filter(extraction_id=query)[0]
        else:
            metadata = VariousMetadata.objects.filter(extraction_id=query)[0]

        if not metadata:
            return Response({'message': 'object not found'})

        data = {
            'date_created': metadata.date_created,
            'date_modified': metadata.date_modified,
            'file_path': metadata.file_path,
            'file_name': metadata.file_name,
            'file_extension': metadata.file_extension,
            'file_mime_type': metadata.file_mime_type
        }

        metadata = GetMetadataSerializer(data).data
        return Response(metadata)


class LimitPagination(MultipleModelLimitOffsetPagination):
    default_limit = 5


class QueryMetadata(ObjectMultipleModelAPIView):
    pagination_class = LimitPagination

    def get_querylist(self):
        tag = self.request.query_params['tag'].lower()
        value = self.request.query_params['value'].lower()

        tags = {'date_created', 'date_modified', 'file_path',
                'file_name', 'file_extension', 'file_mime_type'}

        if tag not in tags:
            querylist = (
                {'queryset': ImageMetadata.objects.filter(
                    file_name="tagnotfounddummyqs"
                ), 'serializer_class': GetMetadataSerializer},
            )
            return querylist

        image_set = {'arw', 'jpg', 'png', 'gif', 'tif', 'heic'}
        video_set = {'mp4', 'mts', 'mkv', 'mov', 'avi', 'wmv', 'flv', 'webm', 'mpeg-2', 'avchd'}
        audio_set = {'m4a', 'mp3', 'wav', 'wma', 'aac', 'flac'}
        text_set = {'txt', 'pdf', 'py', 'c', 'md', 'csv', 'odt', 'odt' 'doc', 'docx'}

        # Because I have separate models for file type, if file_extension or mime_type
        # are requested, I just need to check the type or extension and query only the right model.
        if 'file_extension' in tag or 'file_mime_type' in tag:
            if value in image_set or 'image' in value:
                query_metadata = ImageMetadata.objects.filter(**{tag: value})
            elif value in video_set or 'video' in value:
                query_metadata = VideoMetadata.objects.filter(**{tag: value})
            elif value in audio_set or 'audio' in value:
                query_metadata = AudioMetadata.objects.filter(**{tag: value})
            elif value in text_set or 'text' in value:
                query_metadata = TextMetadata.objects.filter(**{tag: value})
            else:
                query_metadata = VariousMetadata.objects.filter(**{tag: value})

            querylist = (
                {'queryset': query_metadata, 'serializer_class': GetMetadataSerializer},
            )
            return querylist

        image_metadata = ImageMetadata.objects.filter(**{tag: value})
        video_metadata = VideoMetadata.objects.filter(**{tag: value})
        audio_metadata = AudioMetadata.objects.filter(**{tag: value})
        text_metadata = TextMetadata.objects.filter(**{tag: value})
        various_metadata = VariousMetadata.objects.filter(**{tag: value})

        querylist = (
            {'queryset': image_metadata, 'serializer_class': ImageMetadataSerializer},
            {'queryset': video_metadata, 'serializer_class': VideoMetadataSerializer},
            {'queryset': audio_metadata, 'serializer_class': AudioMetadataSerializer},
            {'queryset': text_metadata, 'serializer_class': TextMetadataSerializer},
            {'queryset': various_metadata, 'serializer_class': VariousMetadataSerializer},
        )

        return querylist
