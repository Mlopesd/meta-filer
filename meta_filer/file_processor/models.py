from django.contrib.auth.models import AbstractUser
from django.db import models


class ImageMetadata(models.Model):
    extraction_id = models.CharField(max_length=100)
    date_created = models.DateField(verbose_name='Created date')
    date_modified = models.DateField(verbose_name='Last modified')
    file_path = models.CharField(max_length=140)
    file_name = models.CharField(max_length=140)
    file_extension = models.CharField(max_length=140)
    file_mime_type = models.CharField(max_length=140)

    def save(self, **kwargs):
        super(ImageMetadata, self).save(**kwargs)
        if not self.extraction_id:
            self.extraction_id = '{}_{}'.format('IMG', self.pk)
            self.save()

    def __str__(self):
        return f'{self.file_name} ({self.file_mime_type})'


class TextMetadata(models.Model):
    extraction_id = models.CharField(max_length=140)
    date_created = models.DateField(verbose_name='Created date')
    date_modified = models.DateField(verbose_name='Last modified')
    file_path = models.CharField(max_length=140)
    file_name = models.CharField(max_length=140)
    file_extension = models.CharField(max_length=140)
    file_mime_type = models.CharField(max_length=140)

    def save(self, **kwargs):
        super(TextMetadata, self).save(**kwargs)
        if not self.extraction_id:
            self.extraction_id = '{}_{}'.format('TXT', self.id)
            self.save()

    def __str__(self):
        return f'{self.file_name} ({self.file_mime_type})'


class AudioMetadata(models.Model):
    extraction_id = models.CharField(max_length=140)
    date_created = models.DateField(verbose_name='Created date')
    date_modified = models.DateField(verbose_name='Last modified')
    file_path = models.CharField(max_length=140)
    file_name = models.CharField(max_length=140)
    file_extension = models.CharField(max_length=140)
    file_mime_type = models.CharField(max_length=140)

    def save(self, **kwargs):
        super(AudioMetadata, self).save(**kwargs)
        if not self.extraction_id:
            self.extraction_id = '{}_{}'.format('AUDIO', self.id)
            self.save()

    def __str__(self):
        return f'{self.file_name} ({self.file_mime_type})'


class VideoMetadata(models.Model):
    extraction_id = models.CharField(max_length=140)
    date_created = models.DateField(verbose_name='Created date')
    date_modified = models.DateField(verbose_name='Last modified')
    file_path = models.CharField(max_length=140)
    file_name = models.CharField(max_length=140)
    file_extension = models.CharField(max_length=140)
    file_mime_type = models.CharField(max_length=140)

    def save(self, **kwargs):
        super(VideoMetadata, self).save(**kwargs)
        if not self.extraction_id:
            self.extraction_id = '{}_{}'.format('VIDEO', self.id)
            self.save()

    def __str__(self):
        return f'{self.file_name} ({self.file_mime_type})'


class VariousMetadata(models.Model):
    extraction_id = models.CharField(max_length=140)
    date_created = models.DateField(verbose_name='Created date')
    date_modified = models.DateField(verbose_name='Last modified')
    file_path = models.CharField(max_length=140)
    file_name = models.CharField(max_length=140)
    file_extension = models.CharField(max_length=140)
    file_mime_type = models.CharField(max_length=140)

    def save(self, **kwargs):
        super(VariousMetadata, self).save(**kwargs)
        if not self.extraction_id:
            self.extraction_id = '{}_{}'.format('VARIOUS', self.id)
            self.save()

    def __str__(self):
        return f'{self.file_name} ({self.file_mime_type})'


class User(AbstractUser):
    pass
