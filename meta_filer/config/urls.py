from django.contrib import admin
from django.urls import include, path

urlpatterns = [
    path('admin/', admin.site.urls),
    path('file_processor/', include('meta_filer.file_processor.urls', namespace='file_processor')),
]
